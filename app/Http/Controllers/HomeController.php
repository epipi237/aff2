<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\pending;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.dashboard');
    }

    public function logout() {
        Auth::logout();

        return redirect()->route('login');
    }
    public function approveAccount(){
        $pendings=pending::where('state',0)->paginate(20);
        return view('approveAccount',compact('pendings'));
    }
}
