<?php

namespace App\Http\Controllers;

use App\creative;
use Illuminate\Http\Request;

class CreativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $creatives = creative::select('image')->get();
        return view('dashboard.creatives', ["creatives" => $creatives]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.create_creative');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$request->validate([
            'text' => 'required'
        ]);*/

        if ($request->hasFile('file')) {

            $file = $request->file;

            $destinationPath = public_path().'/uploads/';
            $filename = time().'.'.$file->getClientOriginalExtension();
            $path = $file->move($destinationPath, $filename);
        }
        $creative = new creative();
        $creative->name = "test";
        $creative->description = $request->description;
        $creative->text = $request->text;
        $creative->url = "http://test.com";
        $creative->date = date('Y-m-d H:i:s');
        $creative->status = "clear";
        $creative->image = $filename;
        $creative->save();

        return redirect()->route('creative.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
