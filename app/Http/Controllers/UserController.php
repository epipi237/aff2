<?php

namespace App\Http\Controllers;

use App\pending;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function showUsers()
    {
        $users = User::select('id','name', 'email')->where('role', 'user')->get();
        return view('dashboard.users', ['users' => $users]);
    }

    public function searchUsers(Request $request)
    {
        $search = trim($request->search);
        $users = User::select('id','name', 'email')->where('name','LIKE', '%'.$search.'%')
            ->orWhere('email','LIKE', '%'.$search.'%')->get();

        return view('dashboard.users', ['users' => $users]);
    }

    public function showPending()
    {
        $penders = pending::select('id','name', 'email', 'state')->where('state', 0)->get();
        return view('dashboard.pending_users', ['penders' => $penders]);
    }

    public function legiticeUser(Request $request)
    {
        $pender_id = $request->pender_id;
        $current_pender = pending::where('id', $pender_id)->first();

        //copying pender to users table
        $user = new User();
        $user->name = $current_pender->name;
        $user->email = $current_pender->email;
        $user->password = $current_pender->password;
        $user->role = "user";
        $user->save();

        //update state from pending table
        $current_pender->state = 1;
        $current_pender->update();

        return redirect()->back();

    }

    public function searchPenders(Request $request)
    {
        $search = trim($request->search);
        $penders = pending::select('id','name', 'email', 'state')->where('name','LIKE', '%'.$search.'%')
                            ->orWhere('email','LIKE', '%'.$search.'%')->get();

        return view('dashboard.pending_users', ['penders' => $penders]);
    }

}
