@extends('layouts.frontend')

@section('css')
  <style type="text/css">
    .uppercase {
      text-transform: uppercase;
    }
  </style>
@endsection

@section('content')
<header id="header">
  <div class="container">
    <div class="col-sm-6">
      <div class="logo"><a href="#"><img alt="" src="{{ asset('img/logo_1.png') }}"></a></div>
    </div>
    <div class="col-sm-6">
      <ul class="social-icon">
        <li><a href="#"><i class="fa fa-facebook"></i><span></span></a></li>
        <li><a href="#"><i class="fa fa-twitter"><span></span></i></a></li>
        <li><a href="#"><i class="fa fa-linkedin"><span></span></i></a></li>
      </ul>
    </div>
    <div class="col-xs-12">
      <h1>Earning money with us never been so easy</h1>
      <p>Our system not only offers the highest commissions, it also provides the easiest tools to monetize your web traffic and earn money with your website.</p>
      <a href="#affiliate" class="button scroll">affiliate with us</a> </div>
  </div>
</header>

<!-- =========================
     Section2 - Sponsors  
============================== -->
<div id="sponsors">
  <div class="container">
    <div class="col-xs-12 col-sm-6 col-md-3 column"> <a class="sponsor" href="#"><img alt="" src="{{ asset('img/logo_2.png') }}"></a> </div>
    <div class="col-xs-12 col-sm-6 col-md-3 column"> <a class="sponsor" href="#"><img alt="" src="{{ asset('img/logo_3.png') }}"></a> </div>
    <div class="col-xs-12 col-sm-6 col-md-3 column"> <a class="sponsor" href="#"><img alt="" src="{{ asset('img/logo_4.png') }}"></a> </div>
    <div class="col-xs-12 col-sm-6 col-md-3 column"> <a class="sponsor" href="#"><img alt="" src="{{ asset('img/logo_5.png') }}"></a> </div>
  </div>
</div>

<!-- =========================
     Section3 - We Build  
============================== -->
<section id="we-build">
  <div class="container reveal-bottom-fade"> 
    <!-- Title -->
    <div class="col-xs-12 text-center">
      <h2 class="uppercase">We build an easy system and a quality relationship with our affiliates and clients</h2>
      <div class="seprator less"></div>
    </div>
    <!-- /End Title --> 
    <!-- Build Box -->
    <div class="col-xs-12 col-md-4">
      <h3 class="uppercase">Affiliates</h3>
      <p>We offer technological tools, experience and quality services so that our affiliates earn more money in an easier way.</p>
    </div>
    <div class="col-xs-12 col-md-4">
      <h3 class="uppercase">Company</h3>
      <p>We continue to improve and implement quality solutions and services day after day for the benefit of our affiliates and customers.</p>
    </div>
    <div class="col-xs-12 col-md-4">
      <h3 class="uppercase">Clients</h3>
      <p>Our clients obtain quality tourist services, with the highest standards in the industry and recommend their website and our services to other users.</p>
    </div>
    <!-- /End Build Box --> 
    <!-- Approach -->
    <div class="col-xs-12 text-center figure reveal-bottom-fade"> <img alt="" src="{{ asset('img/icons_1_2_3.png') }}">
      <h3 class="figure-title">OUR BUSINESS APPROACH IS SIMPLE AND EFFECTIVE</h3>
      <p>IT WILL ALLOW YOU TO MAKE A LOT OF MONEY WITH YOUR PAGE EVEN WHILE SLEEPING</p>
    </div>
    <!-- /End Approach --> 
  </div>
</section>

<!-- =========================
     Section3 - What We Do   
============================== -->
<section id="what-we-do">
  <div class="container"> 
    <!-- Video -->
    <div class="col-md-6 figure reveal-bottom-fade">
      <img alt="" src="{{ asset('img/IMAGE_1.png') }}" width="100%">
    </div>
    <!-- /End Video --> 
    <!-- Right -->
    <div class="col-md-6 reveal-right-fade">
      <h2>Why are affiliate systems one of the best options to win money on the internet?</h2>
      <div class="seprator"></div>
      <p>Affiliate systems allow you to sell any product through your website in a very simple and effective way, with the support and technology of a leading company. The most important recommendations when choosing a partner to monetize your blog or website are:</p>
      <ul class="services-list">
        <li>That the product or service they offer is of high quality.</li>
        <li>Make your tools and advertising easy to install on your website.</li>
        <li>That the commissions are competitive and the payment system agile and safe.</li>
      </ul>
    </div>
    <!-- /End Right --> 
  </div>
</section>

<!-- =========================
     Section4 - Powerful Tools   
============================== -->
<section id="powerful-tools">
  <div class="container"> 
    <!-- Title -->
    <div class="col-xs-12 text-center reveal-bottom-fade">
      <h2 class="uppercase">Simple and powerful tools that allow you to sell so much more every day</h2>
      <div class="seprator"></div>
      <p class="padd-both">Each of these tools is planned and implemented to maximize each sales opportunity efficiently in the shortest possible time, making you earn more money in less time.</p>
    </div>
    <!-- /End Title -->
    <div class="col-md-6 column"> 
      <!-- Tool -->
      <div class="tools-box reveal-right-delay">
        <div class="tool-circle one-tone"><i class="fa fa-rocket"></i></div>
        <h3 class="uppercase">Quality Websites</h3>
        <p>The landing pages where your referrals visit have been designed for the visitor to book very easily. Once your referred reservation is sent an almost instantaneous notification.</p>
      </div>
      <!-- /End Tool --> 
      
      <!-- Tool -->
      <div class="tools-box reveal-right-delay">
        <div class="tool-circle two-tone"><i class="fa  fa-envelope"></i></div>
        <h3 class="uppercase">Newsletters</h3>
        <p>We issue newsletters with tips, ideas and new tools so that our members have the latest sales tools to maximize their profits through their website.</p>
      </div>
      <!-- /End Tool --> 
      
      <!-- Tool -->
      <div class="tools-box reveal-right-delay">
        <div class="tool-circle three-tone"><i class="fa fa-newspaper-o"></i></div>
        <h3 class="uppercase">Advertising Banners</h3>
        <p>We have an excellent team that creates effective but non-invasive advertising, easy to insert on your website and that is periodically updated to offer great offers and improve your sales pace.</p>
      </div>
      <!-- /End Tool --> 
    </div>
    <div class="col-md-6 column"> 
      <!-- Tool -->
      <div class="tools-box reveal-right-delay">
        <div class="tool-circle one-tone"><i class="fa fa-tasks"></i></div>
        <h3 class="uppercase">Powerful Tools</h3>
        <p>We provide non-invasive advertising, links. statistics and constant monitoring tools that are very easy to use that allow you to include our tools on your website with very few clicks.</p>
      </div>
      <!-- /End Tool --> 
      
      <!-- Tool -->
      <div class="tools-box reveal-right-delay">
        <div class="tool-circle two-tone"><i class="fa fa-link"></i></div>
        <h3 class="uppercase">Referral links</h3>
        <p>Each visit counts, that is the reasson why our powerful code keeps your referral's visit for up to 30 days, and if you buy it in that period of time, you will receive your sales commission safely.</p>
      </div>
      <!-- /End Tool --> 
      
      <!-- Tool -->
      <div class="tools-box reveal-right-delay">
        <div class="tool-circle three-tone"><i class="fa fa-bar-chart"></i></div>
        <h3 class="uppercase">Statistics</h3>
        <p>You can monitor the progress of your campaigns in easy way, manage your advertising, create links and accurately measure your successful visits and generate sales reports.</p>
      </div>
      <!-- /End Tool --> 
    </div>
  </div>
</section>

<!-- =========================
     Section5 - Popular   
============================== -->
<section id="popular">
  <div class="container"> 
    <!--Popular Lef -->
    <div class="col-md-7">
      <h2 class="uppercase">Efficient and simple dashboard can consult you anywhere even on your mobile.</h2>
      <div class="seprator"></div>
      <p>Our dashboard with a friendly graphic interface will provides you all the functionality you want, you can easily monitor the performance of your campaigns and plan new strategies to help you increase your benefits.</p>
    </div>
    <!-- /End Popular Left --> 
    <!-- Popular Right -->
    <div class="col-md-5 laptop-img stat-shadow"><img class="reveal-top" alt="" src="{{asset('massketing/world-map/img/stats.png')}}"></div>
    <!-- /End Popular Right --> 
  </div>
</section>

<!-- =========================
     Section6 - Our Client   
============================== -->
<section id="our-client">
  <div class="container"> 
    <!-- Title -->
    <div class="col-xs-12 reveal-bottom-fade">
      <h2 class="uppercase">What our affiliates think about working with our team?</h2>
      <div class="seprator less"></div>
    </div>
    <!-- /End Title --> 
    <!-- Client -->
    <div class="col-md-4 column reveal-left-fade"> <img alt="" class="client-img client-one-tone" src="{{ asset('img/PIC_1.png') }}">
      <p><i>"Affiliate managers are always willing to give their best and open to know our business model better to get conversions with very well planned marketing material. We hope to continue working closely with TRANSFERS-USA next year to increase our benefits."</i></p>
      <h3 class="uppercase">Daniel Thompson</h3>
      <h5 class="client-one-tone">Transfers-usa.com affiliate</h5>
    </div>
    <!-- /End Client --> 
    
    <!-- Client -->
    <div class="col-md-4 column reveal-bottom-fade"> <img alt="" class="client-img client-two-tone" src="{{ asset('img/PIC_2.png') }}">
      <p><i>"We have been working with the Tranfers-usa.com team for 8 months and we can say with total sincerity that they have helped us to take off our campaigns and revenues, and that they have always provided us with a constant and complete service."</i></p>
      <h3 class="uppercase">Allison Hannigan</h3>
      <h5 class="client-two-tone">Transfers-usa.com affiliate</h5>
    </div>
    <!-- /End Client --> 
    
    <!-- Client -->
    <div class="col-md-4 column reveal-right-fade"> <img alt="" class="client-img client-three-tone" src="{{ asset('img/PIC_3.png') }}">
      <p><i>"With transfer-usa you have all the support of your affiliate operators! They respond quickly to your emails, listen to your concerns and requests, and that gives more value to the relationship. We work with other affiliate programs and teams, and transfers-usa is among the best in the industry."</i></p>
      <h3 class="uppercase">Tyler Gwynn</h3>
      <h5 class="client-three-tone">Transfers-usa.com affiliate</h5>
    </div>
    <!-- /End Client --> 
  </div>
</section>

<!-- =========================
     Section7 - Affiliate   
============================== -->
<section id="affiliate">
  <div class="container reveal-bottom-fade"> 
    <!-- Title -->
    <div class="col-xs-12">
      <h2 class="uppercase">Join now one of the best affiliate programs in the tourism sector and start generating sales immediately.</h2>
      <div class="seprator"></div>
      <p class="padd-both">Now is the best time to join one of the most effective and profitable affiliate programs in the tourism sector, you can fill out this form and start generating benefits for your project.</p>
    </div>
    <!-- /End Title --> 

    <div class="row">
      <div class="col-xs-12 col-md-6 col-md-push-3">
        <h1>!Affiliate Now!</h1>
        <p>Complete the form below and start earning more with your website</p>
      </div>
    </div>

    <!-- Form -->
    <div class=" col-xs-12 col-md-6 col-md-push-3 form-holder"> 
      <!-- Begin Signup Form --> 
      <!-- Newsletter Success -->
      <div id="newsletter-success"><i class="fa fa-envelope" aria-hidden="true"></i> <br>
        Confirmation email sent, Check your Inbox !</div>
      <!-- Newsletter Error -->
      <div id="newsletter-error">
        <ul>
          <li> <i class="fa fa-times" aria-hidden="true"></i><br>
            <label for="email" class="error" id="validation_error"></label>
          </li>
        </ul>
      </div>
      <form id="subscribe" class="cmxform" name="subscribe" method="post" novalidate action="">
        <div class="row">
          <div class="col-md-6 column">
            <div class="input-wrapper">
              <input type="text" name="name" id="name" value="" required class="newsletter-input" placeholder="Name" />
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="col-md-6 column">
            <div class="input-wrapper">
              <input type="email" name="email" id="email" value="" required class="newsletter-input" placeholder="Email" />
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 column">
            <div class="input-wrapper">
              <input type="password" name="password" id="password" value="" required class="newsletter-input" placeholder="Password" />
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="col-md-6 column">
            <div class="input-wrapper">
              <input type="password" name="confirm_password" id="confirm-password" value="" required class="newsletter-input" placeholder="Confirm Password" />
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-md-push-3">
            <div class="input-wrapper">
              <input id="submit" type="submit" name="submit" value="Enviar" class="form-button" data-loading-text="Loading..." />
              <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <input type="checkbox" style="width: 0 !important; height: 0 !important" />
            <label>We accept the <a href="">terms of the agreement</a> and the <a href="">privacy policies</a>
            </label>
          </div>
        </div>
      </form>
    </div>
    <!-- /End Signup Form --> 
  </div>
</section>
@endsection