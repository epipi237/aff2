<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Lumino - Login</title>
	<link href="{{ asset('lumino/lumino/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('lumino/lumino/css/datepicker3.css') }}" rel="stylesheet">
	<link href="{{ asset('lumino/lumino/css/styles.css') }}" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<div class="row">
	@if(Session::has('success'))
	<p align="center" class="alert alert-success">{!!session('success')!!}</p>
	@endif
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">

			@yield('content')
			
		</div><!-- /.col-->
	</div><!-- /.row -->	
	

<script src="{{ asset('lumino/lumino/js/jquery-1.11.1.min.js') }}"></script>
<script src="{{ asset('lumino/lumino/js/bootstrap.min.js') }}"></script>
</body>
</html>
