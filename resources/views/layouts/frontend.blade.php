<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Massketing</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
@yield('css')
<!-- Bootstrap -->
<link href="{{asset('massketing/world-map/css/bootstrap.min.css')}}" rel="stylesheet">

<!-- Font Awesome Icons -->
<link href="{{asset('massketing/world-map/css/font-awesome.min.css')}}" rel="stylesheet">

<!-- Menu Corner Morph -->
<link rel="stylesheet" type="text/css" href="{{asset('massketing/world-map/css/cornermorph.css')}}" />

<!-- Custom Style Sheet -->
<link href="{{asset('massketing/world-map/css/style.css')}}" rel="stylesheet">

<!--<link rel="shortcut icon" href="img/favicon.png">-->

</head>

<body>

<!-- =========================
     Pre-loader 
============================== -->

<div id="loading">
  <div id="loading-center">
    <div id="loading-center-absolute">
      <div id="object"></div>
    </div>
  </div>
</div>
<div class="menu-wrap">
  <nav class="menu">
    <ul class="link-list">
      <li><a class="scroll" href="#header">Massketing Intro</a></li>
      <li><a class="scroll" href="#we-build">Building Relationship</a></li>
      <li><a class="scroll" href="#what-we-do">What We Do</a></li>
      <li><a class="scroll" href="#powerful-tools">Powerful Tools</a></li>
      <li><a class="scroll" href="#popular">Real Time Reports</a></li>
      <li><a class="scroll" href="#our-client">Our Clients</a></li>
      <li><a class="scroll" href="#affiliate">Get Affialiate</a></li>
    </ul>
  </nav>
</div>
<button class="menu-button" id="open-button"><i class="fa fa-bars" aria-hidden="true"></i></button>
<div class="content-wrap"></div>
<!-- =========================
     Background Particles 
============================== -->
<div id="particles-js"></div>

<!-- =========================
     Section1 - Header   
============================== -->
@yield('content')

<!-- =========================
     Section8 - Footer  
============================== -->
<footer id="copy-right">
  <div class="container" style="margin-top: 5%">
    <div class="row">
      <div class="col-md-3 column"> 
        <ul class="social-icon">
          <li><a href="#"><i class="fa fa-envelope"></i><span></span></a></li>
          <li><a href="#"><i class="fa fa-facebook"></i><span></span></a></li>
          <li><a href="#"><i class="fa fa-google-plus"></i><span></span></a></li>
          <li><a href="#"><i class="fa fa-rss"></i><span></span></a></li>
          <li><a href="#"><i class="fa fa-twitter"><span></span></i></a></li>
          <li><a href="#"><i class="fa fa-youtube"><span></span></i></a></li>
        </ul>
      </div>
      <div class="col-md-3 column"> 
        <ul style="list-style-type: none">
          <li>- About Us</li>
          <li>- Contact Us</li>
        </ul>
      </div>
      <div class="col-md-3 column"> 
        <ul style="list-style-type: none">
          <li>- Transfer-usa.com</li>
          <li>- Affiliates Blog</li>
        </ul>
      </div>
      <div class="col-md-3 column"> 
        end images
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 column">
        <p>
          Copyright &copy; 2018. Transfer USA Logo is registered trademark of XCA Travel, S.A. de C.V
          <br >
          <a href="">Terms and Conditions - Privacy Policy</a>
        </p>
      </div>
    </div>
  </div>
</footer>

<!-- =========================
     Scripts   
============================== --> 
<!-- Jquery --> 
<script src="{{asset('massketing/world-map/js/jquery.min.js')}}"></script> 

<!-- Jquery Easing --> 
<script src="{{asset('massketing/world-map/js/jquery.easing.min.js')}}"></script> 

<!-- Bootstrap --> 
<script type="text/javascript" src="{{asset('massketing/world-map/js/bootstrap.min.js')}}"></script> 

<!-- Validate JS --> 
<script src="{{asset('massketing/world-map/js/jquery.validate.min.js')}}"></script> 

<!-- Ajax Form JS --> 
<script src="{{asset('massketing/world-map/js/jquery.form.js')}}"></script> 

<!-- Retina Ready --> 
<script src="{{asset('massketing/world-map/js/retina.min.js')}}"></script> 

<!-- Background Particles --> 
<script src="{{asset('massketing/world-map/js/particles.js')}}"></script> 
<script src="{{asset('massketing/world-map/js/particles-config.js')}}"></script> 

<!-- Scroll Reveal --> 
<script src="{{asset('massketing/world-map/js/scrollreveal.min.js')}}"></script> 

<!-- Menu Corner Morph --> 
<script src="{{asset('massketing/world-map/js/classie.js')}}"></script> 
<script src="{{asset('massketing/world-map/js/cornermorph.js')}}"></script> 

<!-- Custom --> 
<script src="{{asset('massketing/world-map/js/custom.js')}}"></script>
@yield('js')
</body>
</html>
