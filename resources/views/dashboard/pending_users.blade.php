@extends('layouts.master')

@section('header')
    <div class="row">
        <ol class="breadcrumb">
            <li>
                <a href="#"><em class="fa fa-home"></em></a>
            </li>
            <li class="active">Dashboard</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
    </div><!--/.row-->
@endsection

@section('content')

    <div class="panel panel-container">

        <form class="form-inline" method="POST" action="/search_penders">
            {{ csrf_field() }}
            <div class="form-group mb-2">
                <input type="text" name="search" class="form-control" placeholder="Search user by email" >
            </div>

            <button type="submit" class="btn btn-primary mb-2">search</button>
        </form>

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">email</th>
                <th scope="col">Handle</th>
            </tr>
            </thead>
            <tbody>
            @foreach($penders as $pender)
                <tr>
                    <th scope="row">{{ $pender->id }}</th>
                    <td>{{ $pender->name }}</td>
                    <td>{{ $pender->email }}</td>
                    <td>
                        @if($pender->state == 0)
                            <form method="POST" action="/legitice_user">
                                {{ csrf_field() }}
                                <input type="hidden" name="pender_id" value="{{ $pender->id }}">
                                <button class="btn btn-primary">validate</button>
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>

        @include('partials.footer')
    </div><!--/.row-->

@endsection