@extends('layouts.master')

@section('header')
    <div class="row">
        <ol class="breadcrumb">
            <li>
                <a href="#"><em class="fa fa-home"></em></a>
            </li>
            <li class="active">Dashboard</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
    </div><!--/.row-->
@endsection

@section('content')

    <div class="panel panel-container">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-2">

                    <form role="form" method="POST" action="{{ route('creative.store') }}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label>Text Input</label>
                            <input class="form-control" name="text" value="{{old('name')}}" placeholder="Placeholder" required>
                            @if ($errors->has('text'))
                                <p class="help is-danger">{{$errors->first('text')}}</p>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>File input</label>
                            <input type="file" name="file">
                            @if ($errors->has('file'))
                                <p class="help is-danger">{{$errors->first('file')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Text area</label>
                            <textarea class="form-control" value="{{old('description')}}" name="description" rows="3" required></textarea>
                            @if ($errors->has('description'))
                                <p class="help is-danger">{{$errors->first('description')}}</p>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-md btn-primary">Primary</button>
                        </div>

                    </form>

                </div>
            </div>

        </div>

        @include('partials.footer')
    </div><!--/.row-->


@endsection