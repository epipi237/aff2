@extends('layouts.master')

@section('header')
    <div class="row">
        <ol class="breadcrumb">
            <li>
                <a href="#"><em class="fa fa-home"></em></a>
            </li>
            <li class="active">Dashboard</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
    </div><!--/.row-->
@endsection

@section('content')

    <div class="panel panel-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('creative.create') }}" class="btn btn-primary">New creative</a>
                    <br>
                    <br>
                    @foreach($creatives as $creative)
                        <div class="col-md-4">
                            <img src="uploads/{{$creative->image}}" class="img-thumbnail">
                        </div>
                    @endforeach

                </div>
            </div>

        </div>

        @include('partials.footer')
    </div><!--/.row-->


@endsection